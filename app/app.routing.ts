import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { SelectgroupComponent} from "./pages/selectgroup/selectgroup.component";
import { createDiscussionComponent} from "./pages/createDiscussion/createDiscussion.component";

export const routes = [
  { path: "", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "logout", component: LoginComponent },
  { path: "SelGrp", component: SelectgroupComponent },
  {path: "CreDis", component:createDiscussionComponent}
];

export const navigatableComponents = [
  LoginComponent,HomeComponent,SelectgroupComponent,createDiscussionComponent
  ];
