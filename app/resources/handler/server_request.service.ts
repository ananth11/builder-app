import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import * as  base64 from "base-64";
import * as utf8 from "utf8";
import { User } from "../user/user";
import { Deploy } from "../deploy/deploy";

@Injectable()
export class UserService {
  constructor(private http: Http) {}

  register(user: User) {
    let bytes = utf8.encode(JSON.stringify({
        user_name: user.email,
        password: user.password
      }));

    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    headers.append("Cookie", "session_id=" + user.session_id);
    return this.http.post(
      Deploy.apiUrl + "login","resource="+base64.encode(bytes),
      { headers: headers }
    )
    .catch(this.handleErrors);
  }

  login(user: User) {
      var parameters = JSON.stringify({
            email_id: user.email,
            password: user.password
          }); 
      var bytes = utf8.encode(parameters);
      var encodedStr = base64.encode(bytes);
      console.log(encodedStr);

    fetch(Deploy.apiUrl+"login?resource="+encodedStr, {
        method: "POST",
        headers: {
            "Content-Type":"application/x-www-form-urlencoded",
            "resource":encodedStr
        },
        body: encodedStr
      }).then(r => { return r.json(); }).then(function (r) {
        console.log(JSON.stringify(r));
    }).catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
}
