"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var base64 = require("base-64");
var utf8 = require("utf8");
var deploy_1 = require("../deploy/deploy");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.register = function (user) {
        var bytes = utf8.encode(JSON.stringify({
            user_name: user.email,
            password: user.password
        }));
        var headers = new http_1.Headers();
        headers.append("Content-Type", "application/x-www-form-urlencoded");
        headers.append("Cookie", "session_id=" + user.session_id);
        return this.http.post(deploy_1.Deploy.apiUrl + "login", "resource=" + base64.encode(bytes), { headers: headers })
            .catch(this.handleErrors);
    };
    UserService.prototype.login = function (user) {
        var parameters = JSON.stringify({
            email_id: user.email,
            password: user.password
        });
        var bytes = utf8.encode(parameters);
        var encodedStr = base64.encode(bytes);
        console.log(encodedStr);
        return fetch(deploy_1.Deploy.apiUrl + "login?resource=" + encodedStr, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "resource": encodedStr,
                "Cookie": deploy_1.Deploy.session_id
            },
            body: encodedStr
        }).then(function (r) { return r.json(); });
    };
    UserService.prototype.getCommunity = function () {
        require("nativescript-localstorage");
        var session_id = localStorage.getItem('session_id');
        console.log(session_id);
        var parameters = "";
        var bytes = utf8.encode(parameters);
        var encodedStr = base64.encode(bytes);
        console.log(encodedStr);
        return fetch(deploy_1.Deploy.apiUrl + "community?queryId=QUERY_GET_BY_USER_ID&args=", {
            method: "GET",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "resource": encodedStr,
                "Cookie": "session_id=" + session_id
            },
            body: encodedStr
        }).then(function (r) { return r.json(); });
    };
    UserService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error.json()));
        return Rx_1.Observable.throw(error);
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUF3RDtBQUN4RCw4QkFBcUM7QUFDckMsZ0NBQThCO0FBQzlCLGlDQUErQjtBQUMvQixnQ0FBbUM7QUFDbkMsMkJBQTZCO0FBRTdCLDJDQUEwQztBQUcxQyxJQUFhLFdBQVc7SUFDdEIscUJBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO0lBQUcsQ0FBQztJQUVsQyw4QkFBUSxHQUFSLFVBQVMsSUFBVTtRQUNqQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDbkMsU0FBUyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ3JCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN4QixDQUFDLENBQUMsQ0FBQztRQUVOLElBQUksT0FBTyxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7UUFDNUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsbUNBQW1DLENBQUMsQ0FBQztRQUNwRSxPQUFPLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FDbkIsZUFBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLEVBQUMsV0FBVyxHQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQ3hELEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUNyQjthQUNBLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDNUIsQ0FBQztJQUVELDJCQUFLLEdBQUwsVUFBTSxJQUFVO1FBQ1osSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUMxQixRQUFRLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDcEIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQ3hCLENBQUMsQ0FBQztRQUNQLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEMsSUFBSSxVQUFVLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRTFCLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBTSxDQUFDLE1BQU0sR0FBQyxpQkFBaUIsR0FBQyxVQUFVLEVBQUU7WUFDckQsTUFBTSxFQUFFLE1BQU07WUFDZCxPQUFPLEVBQUU7Z0JBQ0wsY0FBYyxFQUFDLG1DQUFtQztnQkFDbEQsVUFBVSxFQUFDLFVBQVU7Z0JBQ3JCLFFBQVEsRUFBQyxlQUFNLENBQUMsVUFBVTthQUM3QjtZQUNELElBQUksRUFBRSxVQUFVO1NBQ2pCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3RDLENBQUM7SUFFRCxrQ0FBWSxHQUFaO1FBQ0csT0FBTyxDQUFFLDJCQUEyQixDQUFFLENBQUM7UUFDdkMsSUFBSSxVQUFVLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksVUFBVSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUUxQixNQUFNLENBQUMsS0FBSyxDQUFDLGVBQU0sQ0FBQyxNQUFNLEdBQUMsOENBQThDLEVBQUU7WUFDdkUsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUU7Z0JBQ0wsY0FBYyxFQUFDLG1DQUFtQztnQkFDbEQsVUFBVSxFQUFDLFVBQVU7Z0JBQ3JCLFFBQVEsRUFBQyxhQUFhLEdBQUMsVUFBVTthQUNwQztZQUNELElBQUksRUFBRSxVQUFVO1NBQ2pCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3RDLENBQUM7SUFFRCxrQ0FBWSxHQUFaLFVBQWEsS0FBZTtRQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMxQyxNQUFNLENBQUMsZUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDLEFBL0RELElBK0RDO0FBL0RZLFdBQVc7SUFEdkIsaUJBQVUsRUFBRTtxQ0FFZSxXQUFJO0dBRG5CLFdBQVcsQ0ErRHZCO0FBL0RZLGtDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBIdHRwLCBIZWFkZXJzLCBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9odHRwXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anMvUnhcIjtcbmltcG9ydCBcInJ4anMvYWRkL29wZXJhdG9yL2RvXCI7XG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9tYXBcIjtcbmltcG9ydCAqIGFzICBiYXNlNjQgZnJvbSBcImJhc2UtNjRcIjtcbmltcG9ydCAqIGFzIHV0ZjggZnJvbSBcInV0ZjhcIjtcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi91c2VyXCI7XG5pbXBvcnQgeyBEZXBsb3kgfSBmcm9tIFwiLi4vZGVwbG95L2RlcGxveVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVXNlclNlcnZpY2Uge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHt9XG5cbiAgcmVnaXN0ZXIodXNlcjogVXNlcikge1xuICAgIGxldCBieXRlcyA9IHV0ZjguZW5jb2RlKEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgdXNlcl9uYW1lOiB1c2VyLmVtYWlsLFxuICAgICAgICBwYXNzd29yZDogdXNlci5wYXNzd29yZFxuICAgICAgfSkpO1xuXG4gICAgbGV0IGhlYWRlcnMgPSBuZXcgSGVhZGVycygpO1xuICAgIGhlYWRlcnMuYXBwZW5kKFwiQ29udGVudC1UeXBlXCIsIFwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIpO1xuICAgIGhlYWRlcnMuYXBwZW5kKFwiQ29va2llXCIsIFwic2Vzc2lvbl9pZD1cIiArIHVzZXIuc2Vzc2lvbl9pZCk7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0KFxuICAgICAgRGVwbG95LmFwaVVybCArIFwibG9naW5cIixcInJlc291cmNlPVwiK2Jhc2U2NC5lbmNvZGUoYnl0ZXMpLFxuICAgICAgeyBoZWFkZXJzOiBoZWFkZXJzIH1cbiAgICApXG4gICAgLmNhdGNoKHRoaXMuaGFuZGxlRXJyb3JzKTtcbiAgfVxuXG4gIGxvZ2luKHVzZXI6IFVzZXIpIHtcbiAgICAgIHZhciBwYXJhbWV0ZXJzID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgZW1haWxfaWQ6IHVzZXIuZW1haWwsXG4gICAgICAgICAgICBwYXNzd29yZDogdXNlci5wYXNzd29yZFxuICAgICAgICAgIH0pOyBcbiAgICAgIHZhciBieXRlcyA9IHV0ZjguZW5jb2RlKHBhcmFtZXRlcnMpO1xuICAgICAgdmFyIGVuY29kZWRTdHIgPSBiYXNlNjQuZW5jb2RlKGJ5dGVzKTtcbiAgICAgIGNvbnNvbGUubG9nKGVuY29kZWRTdHIpO1xuXG4gICAgcmV0dXJuIGZldGNoKERlcGxveS5hcGlVcmwrXCJsb2dpbj9yZXNvdXJjZT1cIitlbmNvZGVkU3RyLCB7XG4gICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6XCJhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWRcIixcbiAgICAgICAgICAgIFwicmVzb3VyY2VcIjplbmNvZGVkU3RyLFxuICAgICAgICAgICAgXCJDb29raWVcIjpEZXBsb3kuc2Vzc2lvbl9pZFxuICAgICAgICB9LFxuICAgICAgICBib2R5OiBlbmNvZGVkU3RyXG4gICAgICB9KS50aGVuKHIgPT4geyByZXR1cm4gci5qc29uKCk7IH0pXG4gIH1cblxuICBnZXRDb21tdW5pdHkoKSB7XG4gICAgIHJlcXVpcmUoIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiICk7XG4gICAgIGxldCBzZXNzaW9uX2lkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nlc3Npb25faWQnKTtcbiAgICAgY29uc29sZS5sb2coc2Vzc2lvbl9pZCk7XG4gICAgICB2YXIgcGFyYW1ldGVycyA9IFwiXCI7IFxuICAgICAgdmFyIGJ5dGVzID0gdXRmOC5lbmNvZGUocGFyYW1ldGVycyk7XG4gICAgICB2YXIgZW5jb2RlZFN0ciA9IGJhc2U2NC5lbmNvZGUoYnl0ZXMpO1xuICAgICAgY29uc29sZS5sb2coZW5jb2RlZFN0cik7XG5cbiAgICByZXR1cm4gZmV0Y2goRGVwbG95LmFwaVVybCtcImNvbW11bml0eT9xdWVyeUlkPVFVRVJZX0dFVF9CWV9VU0VSX0lEJmFyZ3M9XCIsIHtcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOlwiYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkXCIsXG4gICAgICAgICAgICBcInJlc291cmNlXCI6ZW5jb2RlZFN0cixcbiAgICAgICAgICAgIFwiQ29va2llXCI6XCJzZXNzaW9uX2lkPVwiK3Nlc3Npb25faWRcbiAgICAgICAgfSxcbiAgICAgICAgYm9keTogZW5jb2RlZFN0clxuICAgICAgfSkudGhlbihyID0+IHsgcmV0dXJuIHIuanNvbigpOyB9KVxuICB9XG5cbiAgaGFuZGxlRXJyb3JzKGVycm9yOiBSZXNwb25zZSkge1xuICAgIGNvbnNvbGUubG9nKEpTT04uc3RyaW5naWZ5KGVycm9yLmpzb24oKSkpO1xuICAgIHJldHVybiBPYnNlcnZhYmxlLnRocm93KGVycm9yKTtcbiAgfVxufVxuIl19