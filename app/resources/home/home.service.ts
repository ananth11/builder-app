import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import * as  base64 from "base-64";
import * as utf8 from "utf8";
import { User } from "../user/user";
import { Deploy } from "../deploy/deploy";

@Injectable()
export class HomeService {
  constructor(private http: Http) {}

  getMyDiscussion() {
      var parameters = "community_id:"+Deploy.community_id; 
      var bytes = utf8.encode(parameters);
      var encodedStr = encodeURI(bytes);
      console.log(encodedStr);
    return fetch(Deploy.apiUrl+"discussion?queryId=QUERY_GET_MY&args="+encodedStr, {
        method: "GET",
        headers: {
           "Cookie":"session_id="+Deploy.session_id
        }
      }).then(r => { return r.json(); })
  }

  getDiscussion() {
      var parameters = "community_id:"+Deploy.community_id; 
      var bytes = utf8.encode(parameters);
      var encodedStr = encodeURI(bytes);
      console.log(encodedStr);

    return fetch(Deploy.apiUrl+"discussion?queryId=QUERY_GET_BY_USER_ID&args="+encodedStr, {
        method: "GET",
        headers: {
            "Cookie":"session_id="+Deploy.session_id
        }
      }).then(r => { return r.json(); })
  }

  handleErrors(error: Response) {
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }
}
