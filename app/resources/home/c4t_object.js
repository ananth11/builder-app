"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var C4t_object = (function () {
    function C4t_object() {
        this.quantity = 0.0;
        this.free_quantity = 0.0;
        this.feature_mandi_auction = null;
        this.feature_mandi_billing = null;
        this.feature_communication = null;
        this.feature_discussion = null;
        this.feature_helprequest = null;
        this.feature_askdoubt = null;
        this.feature_business = null;
        this.feature_project_task = null;
        this.feature_opinion = null;
        this.feature_poll = null;
        this.feature_event = null;
        this.feature_survey = null;
        this.feature_meeting = null;
        this.feature_news = null;
        this.feature_article = null;
        this.feature_video = null;
        this.server_url = null;
        this.authentication_server_url = null;
        this.home_delivery_free_threshold = null;
        this.home_delivery_min_value = null;
        this.upload_status = null;
        this.fin_year = null;
        this.weight_upload_status = null;
        this.weight = null;
        this.average_weight = null;
        this.rate = null;
        this.party = null;
        this.party_full_name = null;
        this.weight_status = null;
        this.entry_date = null;
        this.bill_status = null;
        this.patti_status = null;
        this.deleted = null;
        this.backup = null;
        this.backup_patti = null;
        this.weights = null;
        this.expiry_date = null;
        this.arrivals = null;
        this.arrival_updated = null;
        this.onion_arrivals = null;
        this.potato_arrivals = null;
        this.onion_unsold = null;
        this.potato_unsold = null;
    }
    return C4t_object;
}());
exports.C4t_object = C4t_object;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYzR0X29iamVjdC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImM0dF9vYmplY3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQTtJQUFBO1FBd0NNLGFBQVEsR0FBRyxHQUFHLENBQUM7UUFDZixrQkFBYSxHQUFHLEdBQUcsQ0FBQztRQTRCcEIsMEJBQXFCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLDBCQUFxQixHQUFHLElBQUksQ0FBQztRQUM3QiwwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDN0IsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQzFCLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLDhCQUF5QixHQUFHLElBQUksQ0FBQztRQUNqQyxpQ0FBNEIsR0FBRyxJQUFJLENBQUM7UUFDcEMsNEJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBNEIvQixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1QixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2QsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsU0FBSSxHQUFHLElBQUksQ0FBQztRQUNaLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDYixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixXQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ2QsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsWUFBTyxHQUFHLElBQUksQ0FBQztRQVdmLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBS25CLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFFdkIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFDdkIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsa0JBQWEsR0FBRyxJQUFJLENBQUM7SUFrQjNCLENBQUM7SUFBRCxpQkFBQztBQUFELENBQUMsQUEzS0QsSUEyS0M7QUEzS1ksZ0NBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgQzR0X29iamVjdCB7XG4gICAgICBpZCA6IHN0cmluZztcbiAgICAgIG5hbWUgOiBzdHJpbmc7XG4gICAgICBkb2Jfc3RyIDogc3RyaW5nO1xuICAgICAgZ2VuZGVyIDogc3RyaW5nO1xuICAgICAgYWdlIDogbnVtYmVyO1xuICAgICAgZW1haWxfaWQgOnN0cmluZztcbiAgICAgIHVybDpzdHJpbmc7XG4gICAgICBhc3NpZ25fZGF0ZV9zdHI6c3RyaW5nO1xuICAgICAgZmxhdF9pZDpzdHJpbmc7XG4gICAgICBkZXNjcmlwdGlvbjpzdHJpbmc7XG4gICAgICB0eXBlOnN0cmluZztcbiAgICAgIGNvbW11bml0eV90eXBlOnN0cmluZztcbiAgICAgIHN1Yl90eXBlOnN0cmluZztcbiAgICAgIGNvbW11bml0eV9pZDpzdHJpbmc7XG4gICAgICB1c2VyX2lkOnN0cmluZztcbiAgICAgIHJlZmVyZW5jZV9pZDpzdHJpbmc7XG4gICAgICBjb21tdW5pdHlfbmFtZTpzdHJpbmc7XG4gICAgICBjYXRlZ29yeV9uYW1lOnN0cmluZztcbiAgICAgIGNvbXBhbnlfaWQ6c3RyaW5nO1xuICAgICAgY29tcGFueV9uYW1lOnN0cmluZztcbiAgICAgIHBhcmVudF9pZDpzdHJpbmc7XG4gICAgICBzaG9ydF9uYW1lOnN0cmluZztcbiAgICAgIGJyYW5kX25hbWU6c3RyaW5nO1xuICAgICAgbW9iaWxlX25vOnN0cmluZztcbiAgICAgIG9iamVjdF90eXBlOnN0cmluZztcbiAgICAgIHN0YXR1czpzdHJpbmc7XG4gICAgICBtcnBfcHJpY2U6YW55O1xuICAgICAgZGlzdHJpYnV0ZXJfcHJpY2U6YW55O1xuICAgICAgZGVhbGVyX3ByaWNlOmFueTtcbiAgICAgIGN1c3RvbWVyX3ByaWNlOmFueTtcbiAgICAgIHVuaXQ6c3RyaW5nO1xuICAgICAgcmVmZXJlbmNlX25hbWU6c3RyaW5nO1xuICAgICAgY3JlYXRpb25fdGltZTphbnk7XG4gICAgICB2YXQ6YW55O1xuICAgICAgY3VycmVudF9zdG9jazphbnk7XG4gICAgICBzZXJ2aWNlX3RheDphbnk7XG4gICAgICBzZXJ2aWNlX2NoYXJnZXNfaG9tZV9kZWxpdmVyeTphbnk7XG4gICAgICBzZXJ2aWNlX2NoYXJnZXNfdGFrZWF3YXk6YW55O1xuICAgICAgc2VydmljZV9jaGFyZ2VzX29uX3RhYmxlOmFueTtcbiAgICAgIHF1YW50aXR5ID0gMC4wO1xuICAgICAgZnJlZV9xdWFudGl0eSA9IDAuMDtcbiAgICAgIGZlYXR1cmVfbm9uX3ZlZzpzdHJpbmc7XG4gICAgICBmZWF0dXJlX3Byb2R1Y3RfYXZhaWxhYmxlOnN0cmluZztcbiAgICAgIGZlYXR1cmVfYWN0aXZlOnN0cmluZztcbiAgICAgIGZlYXR1cmVfb250YWJsZTpzdHJpbmc7XG4gICAgICBmZWF0dXJlX2hvbWVfZGVsaXZlcnk6c3RyaW5nO1xuICAgICAgZmVhdHVyZV90YWtlYXdheTpzdHJpbmc7XG4gICAgICB2YWxpZF9pbWllOnN0cmluZztcbiAgICAgIHRvdGFsX2l0ZW1zOmFueTtcbiAgICAgIHJlbWFyazpzdHJpbmc7XG4gICAgICBzY2hlbWVfYXZhaWxhYmxlOnN0cmluZztcbiAgICAgIHByb2R1Y3RfdGh1bWJuYWlsX3VybDpzdHJpbmc7XG4gICAgICBwcm9kdWN0X3VybDE6c3RyaW5nO1xuICAgICAgcHJvZHVjdF91cmwyOnN0cmluZztcbiAgICAgIHByb2R1Y3RfdXJsMzpzdHJpbmc7XG4gICAgICBwcm9kdWN0X3VybDQ6c3RyaW5nO1xuICAgICAgYWRkcmVzczpzdHJpbmc7XG4gICAgICBhcmVhOnN0cmluZztcbiAgICAgIGNpdHk6c3RyaW5nO1xuICAgICAgc3RhdGU6c3RyaW5nO1xuICAgICAgbGljZW5zZV9ubzpzdHJpbmc7XG4gICAgICBzaXplOnN0cmluZztcbiAgICAgIHBhdHRlcm46c3RyaW5nO1xuICAgICAgZnJvbV9kYXRlOnN0cmluZztcbiAgICAgIHRvX2RhdGU6c3RyaW5nO1xuICAgICAgY2hlbWlzdF9jb21tdW5pdHlfaWQ6c3RyaW5nO1xuICAgICAgYmxvY2tfbm86c3RyaW5nO1xuICAgICAgbWVtYmVyX3R5cGU6c3RyaW5nO1xuICAgICAgZmVhdHVyZV9tYW5kaV9hdWN0aW9uID0gbnVsbDtcbiAgICAgIGZlYXR1cmVfbWFuZGlfYmlsbGluZyA9IG51bGw7XG4gICAgICBmZWF0dXJlX2NvbW11bmljYXRpb24gPSBudWxsO1xuICAgICAgZmVhdHVyZV9kaXNjdXNzaW9uID0gbnVsbDtcbiAgICAgIGZlYXR1cmVfaGVscHJlcXVlc3QgPSBudWxsO1xuICAgICAgZmVhdHVyZV9hc2tkb3VidCA9IG51bGw7XG4gICAgICBmZWF0dXJlX2J1c2luZXNzID0gbnVsbDtcbiAgICAgIGZlYXR1cmVfcHJvamVjdF90YXNrID0gbnVsbDtcbiAgICAgIGZlYXR1cmVfb3BpbmlvbiA9IG51bGw7XG4gICAgICBmZWF0dXJlX3BvbGwgPSBudWxsO1xuICAgICAgZmVhdHVyZV9ldmVudCA9IG51bGw7XG4gICAgICBmZWF0dXJlX3N1cnZleSA9IG51bGw7XG4gICAgICBmZWF0dXJlX21lZXRpbmcgPSBudWxsO1xuICAgICAgZmVhdHVyZV9uZXdzID0gbnVsbDtcbiAgICAgIGZlYXR1cmVfYXJ0aWNsZSA9IG51bGw7XG4gICAgICBmZWF0dXJlX3ZpZGVvID0gbnVsbDtcbiAgICAgIHNlcnZlcl91cmwgPSBudWxsO1xuICAgICAgYXV0aGVudGljYXRpb25fc2VydmVyX3VybCA9IG51bGw7XG4gICAgICBob21lX2RlbGl2ZXJ5X2ZyZWVfdGhyZXNob2xkID0gbnVsbDtcbiAgICAgIGhvbWVfZGVsaXZlcnlfbWluX3ZhbHVlID0gbnVsbDtcbiAgICAgIHZlaGljbGVfbm86c3RyaW5nO1xuICAgICAgZmFybWVyX25hbWU6c3RyaW5nO1xuICAgICAgbG90OnN0cmluZztcbiAgICAgIHRvdGFsX3F1YW50aXR5OnN0cmluZztcbiAgICAgIHByb2R1Y3RfbmFtZTpzdHJpbmc7XG4gICAgICBldmVudF9kYXRlX3N0cjpzdHJpbmc7XG4gICAgICByZWNvcmRfbm86c3RyaW5nO1xuICAgICAgbWVtYmVyX25hbWU6c3RyaW5nO1xuICAgICAgcmVjb3JkX3N1Yl90eXBlOnN0cmluZztcbiAgICAgIHRvdGFsX2Ftb3VudDpzdHJpbmc7XG4gICAgICBuZXRfYW1vdW50OnN0cmluZztcbiAgICAgIHRvdGFsX3RyYW5zcG9ydF9jaGFyZ2U6c3RyaW5nO1xuICAgICAgdG90YWxfaGlkZGVuX2NoYXJnZXM6c3RyaW5nO1xuICAgICAgdG90YWxfdW5sb2FkaW5nX2NoYXJnZTpzdHJpbmc7XG4gICAgICByZWNvcmRfc3RhdHVzOnN0cmluZztcbiAgICAgIHNvbGQ6c3RyaW5nO1xuICAgICAgcmVtYWluaW5nOnN0cmluZztcbiAgICAgIHRvdGFsX3BhaWRfYW1vdW50OnN0cmluZztcbiAgICAgIGxhc3RfdXBkYXRlX3RpbWU6YW55O1xuICAgICAgbGFzdF9yZWNoYXJnZV90aW1lOnN0cmluZztcbiAgICAgIGxhc3RfY291cGFuX25vOnN0cmluZztcbiAgICAgIGxhc3RfcmVjaGFyZ2VfYW1vdW50OmFueTtcbiAgICAgIGJhbGFuY2VfYW1vdW50OmFueTtcbiAgICAgIGRnX3JlYWRpbmc6YW55O1xuICAgICAgZ3JpZF9yZWFkaW5nOmFueTtcbiAgICAgIGdyYWRlX25hbWU6c3RyaW5nO1xuICAgICAgcGxhY2U6c3RyaW5nO1xuICAgICAgdXBsb2FkX3N0YXR1cyA9IG51bGw7XG4gICAgICBmaW5feWVhciA9IG51bGw7XG4gICAgICB3ZWlnaHRfdXBsb2FkX3N0YXR1cyA9IG51bGw7XG4gICAgICB3ZWlnaHQgPSBudWxsO1xuICAgICAgYXZlcmFnZV93ZWlnaHQgPSBudWxsO1xuICAgICAgcmF0ZSA9IG51bGw7XG4gICAgICBwYXJ0eSA9IG51bGw7XG4gICAgICBwYXJ0eV9mdWxsX25hbWUgPSBudWxsO1xuICAgICAgd2VpZ2h0X3N0YXR1cyA9IG51bGw7XG4gICAgICBlbnRyeV9kYXRlID0gbnVsbDtcbiAgICAgIGJpbGxfc3RhdHVzID0gbnVsbDtcbiAgICAgIHBhdHRpX3N0YXR1cyA9IG51bGw7XG4gICAgICBkZWxldGVkID0gbnVsbDtcbiAgICAgIGJhY2t1cCA9IG51bGw7XG4gICAgICBiYWNrdXBfcGF0dGkgPSBudWxsO1xuICAgICAgd2VpZ2h0cyA9IG51bGw7XG4gICAgICBjb21taXNzaW9uX3ByY3Q6YW55O1xuICAgICAgbWFuZGlfZmVlX3ByY3Q6YW55O1xuICAgICAgbWFuZGlfZmVlX2Ftb3VudDphbnk7XG4gICAgICBjb21taXNzaW9uX2Ftb3VudDphbnk7XG4gICAgICBleHRyYV9jaGFyZ2VfcGVyX2JhZzphbnk7XG4gICAgICBleHRyYV9jaGFyZ2VfYW1vdW50OmFueTtcbiAgICAgIGFtb3VudDphbnk7XG4gICAgICBmaW5hbF9hbW91bnQ6YW55O1xuICAgICAgcGFzc3dvcmQ6c3RyaW5nO1xuICAgICAgbGljZW5zZV90eXBlOnN0cmluZztcbiAgICAgIGV4cGlyeV9kYXRlID0gbnVsbDtcbiAgICAgIG1hbmRpX21haW5fcHJpY2U6YW55O1xuICAgICAgbWFuZGlfc2Vjb25kYXJ5X3ByaWNlOmFueTtcbiAgICAgIHZhbGlkX21hbmRpX21haW5faW1pZTpzdHJpbmc7XG4gICAgICB2YWxpZF9tYW5kaV9zZWNvbmRhcnlfaW1pZTpzdHJpbmc7XG4gICAgICBhcnJpdmFscyA9IG51bGw7XG4gICAgICBhcnJpdmFsX3VwZGF0ZWQgPSBudWxsO1xuICAgICAgYXJyaXZhbF9tZXNzYWdlOnN0cmluZztcbiAgICAgIG9uaW9uX2Fycml2YWxzID0gbnVsbDtcbiAgICAgIHBvdGF0b19hcnJpdmFscyA9IG51bGw7XG4gICAgICBvbmlvbl91bnNvbGQgPSBudWxsO1xuICAgICAgcG90YXRvX3Vuc29sZCA9IG51bGw7XG4gICAgICBwb3RhdG9fYnVzaW5lc3M6c3RyaW5nO1xuICAgICAgb25pb25fYnVzaW5lc3M6c3RyaW5nO1xuICAgICAgYXVjdGlvbl9zdGF0dXM6c3RyaW5nO1xuICAgICAgb25pb25fbWluX3JhdGU7XG4gICAgICBvbmlvbl9tYXhfcmF0ZTtcbiAgICAgIHBvdGF0b19taW5fcmF0ZTtcbiAgICAgIHBvdGF0b19tYXhfcmF0ZTtcbiAgICAgIHBvdGF0b19hcnJpdmFsX21lc3NhZ2U6c3RyaW5nO1xuICAgICAgb25pb25fYXJyaXZhbF9tZXNzYWdlOnN0cmluZztcbiAgICAgIGdyb3VwX25hbWU6c3RyaW5nO1xuICAgICAgcG90YXRvX2Fycml2YWxfbWVzc2FnZV91cGRhdGVkOmFueTtcbiAgICAgIG9uaW9uX2Fycml2YWxfbWVzc2FnZV91cGRhdGVkOmFueTtcbiAgICAgIGNsb3NpbmdfYmFsYW5jZTphbnk7XG4gICAgICBvcGVuaW5nX2JhbGFuY2U6YW55O1xuICAgICAgbGFzdF9pbXBvcnRlZF90aW1lOmFueTtcbiAgICAgIHVzZXJfYXBhcnRtZW50OnN0cmluZztcbiAgICAgIHVzZXJfbmFtZTpzdHJpbmc7XG59XG4iXX0=