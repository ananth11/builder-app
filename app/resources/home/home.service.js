"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
require("rxjs/add/operator/do");
require("rxjs/add/operator/map");
var utf8 = require("utf8");
var deploy_1 = require("../deploy/deploy");
var HomeService = (function () {
    function HomeService(http) {
        this.http = http;
    }
    HomeService.prototype.getMyDiscussion = function () {
        var parameters = "community_id:" + deploy_1.Deploy.community_id;
        var bytes = utf8.encode(parameters);
        var encodedStr = encodeURI(bytes);
        console.log(encodedStr);
        return fetch(deploy_1.Deploy.apiUrl + "discussion?queryId=QUERY_GET_MY&args=" + encodedStr, {
            method: "GET",
            headers: {
                "Cookie": "session_id=" + deploy_1.Deploy.session_id
            }
        }).then(function (r) { return r.json(); });
    };
    HomeService.prototype.getDiscussion = function () {
        var parameters = "community_id:" + deploy_1.Deploy.community_id;
        var bytes = utf8.encode(parameters);
        var encodedStr = encodeURI(bytes);
        console.log(encodedStr);
        return fetch(deploy_1.Deploy.apiUrl + "discussion?queryId=QUERY_GET_BY_USER_ID&args=" + encodedStr, {
            method: "GET",
            headers: {
                "Cookie": "session_id=" + deploy_1.Deploy.session_id
            }
        }).then(function (r) { return r.json(); });
    };
    HomeService.prototype.handleErrors = function (error) {
        console.log(JSON.stringify(error.json()));
        return Rx_1.Observable.throw(error);
    };
    return HomeService;
}());
HomeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], HomeService);
exports.HomeService = HomeService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaG9tZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLHNDQUF3RDtBQUN4RCw4QkFBcUM7QUFDckMsZ0NBQThCO0FBQzlCLGlDQUErQjtBQUUvQiwyQkFBNkI7QUFFN0IsMkNBQTBDO0FBRzFDLElBQWEsV0FBVztJQUN0QixxQkFBb0IsSUFBVTtRQUFWLFNBQUksR0FBSixJQUFJLENBQU07SUFBRyxDQUFDO0lBRWxDLHFDQUFlLEdBQWY7UUFDSSxJQUFJLFVBQVUsR0FBRyxlQUFlLEdBQUMsZUFBTSxDQUFDLFlBQVksQ0FBQztRQUNyRCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BDLElBQUksVUFBVSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBTSxDQUFDLE1BQU0sR0FBQyx1Q0FBdUMsR0FBQyxVQUFVLEVBQUU7WUFDM0UsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUU7Z0JBQ04sUUFBUSxFQUFDLGFBQWEsR0FBQyxlQUFNLENBQUMsVUFBVTthQUMxQztTQUNGLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxDQUFDLElBQU0sTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO0lBQ3RDLENBQUM7SUFFRCxtQ0FBYSxHQUFiO1FBQ0ksSUFBSSxVQUFVLEdBQUcsZUFBZSxHQUFDLGVBQU0sQ0FBQyxZQUFZLENBQUM7UUFDckQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNwQyxJQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUUxQixNQUFNLENBQUMsS0FBSyxDQUFDLGVBQU0sQ0FBQyxNQUFNLEdBQUMsK0NBQStDLEdBQUMsVUFBVSxFQUFFO1lBQ25GLE1BQU0sRUFBRSxLQUFLO1lBQ2IsT0FBTyxFQUFFO2dCQUNMLFFBQVEsRUFBQyxhQUFhLEdBQUMsZUFBTSxDQUFDLFVBQVU7YUFDM0M7U0FDRixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFNLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUN0QyxDQUFDO0lBRUQsa0NBQVksR0FBWixVQUFhLEtBQWU7UUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUMsTUFBTSxDQUFDLGVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUNILGtCQUFDO0FBQUQsQ0FBQyxBQWxDRCxJQWtDQztBQWxDWSxXQUFXO0lBRHZCLGlCQUFVLEVBQUU7cUNBRWUsV0FBSTtHQURuQixXQUFXLENBa0N2QjtBQWxDWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UgfSBmcm9tIFwiQGFuZ3VsYXIvaHR0cFwiO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gXCJyeGpzL1J4XCI7XG5pbXBvcnQgXCJyeGpzL2FkZC9vcGVyYXRvci9kb1wiO1xuaW1wb3J0IFwicnhqcy9hZGQvb3BlcmF0b3IvbWFwXCI7XG5pbXBvcnQgKiBhcyAgYmFzZTY0IGZyb20gXCJiYXNlLTY0XCI7XG5pbXBvcnQgKiBhcyB1dGY4IGZyb20gXCJ1dGY4XCI7XG5pbXBvcnQgeyBVc2VyIH0gZnJvbSBcIi4uL3VzZXIvdXNlclwiO1xuaW1wb3J0IHsgRGVwbG95IH0gZnJvbSBcIi4uL2RlcGxveS9kZXBsb3lcIjtcblxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIEhvbWVTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwKSB7fVxuXG4gIGdldE15RGlzY3Vzc2lvbigpIHtcbiAgICAgIHZhciBwYXJhbWV0ZXJzID0gXCJjb21tdW5pdHlfaWQ6XCIrRGVwbG95LmNvbW11bml0eV9pZDsgXG4gICAgICB2YXIgYnl0ZXMgPSB1dGY4LmVuY29kZShwYXJhbWV0ZXJzKTtcbiAgICAgIHZhciBlbmNvZGVkU3RyID0gZW5jb2RlVVJJKGJ5dGVzKTtcbiAgICAgIGNvbnNvbGUubG9nKGVuY29kZWRTdHIpO1xuICAgIHJldHVybiBmZXRjaChEZXBsb3kuYXBpVXJsK1wiZGlzY3Vzc2lvbj9xdWVyeUlkPVFVRVJZX0dFVF9NWSZhcmdzPVwiK2VuY29kZWRTdHIsIHtcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgIFwiQ29va2llXCI6XCJzZXNzaW9uX2lkPVwiK0RlcGxveS5zZXNzaW9uX2lkXG4gICAgICAgIH1cbiAgICAgIH0pLnRoZW4ociA9PiB7IHJldHVybiByLmpzb24oKTsgfSlcbiAgfVxuXG4gIGdldERpc2N1c3Npb24oKSB7XG4gICAgICB2YXIgcGFyYW1ldGVycyA9IFwiY29tbXVuaXR5X2lkOlwiK0RlcGxveS5jb21tdW5pdHlfaWQ7IFxuICAgICAgdmFyIGJ5dGVzID0gdXRmOC5lbmNvZGUocGFyYW1ldGVycyk7XG4gICAgICB2YXIgZW5jb2RlZFN0ciA9IGVuY29kZVVSSShieXRlcyk7XG4gICAgICBjb25zb2xlLmxvZyhlbmNvZGVkU3RyKTtcblxuICAgIHJldHVybiBmZXRjaChEZXBsb3kuYXBpVXJsK1wiZGlzY3Vzc2lvbj9xdWVyeUlkPVFVRVJZX0dFVF9CWV9VU0VSX0lEJmFyZ3M9XCIrZW5jb2RlZFN0ciwge1xuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgIFwiQ29va2llXCI6XCJzZXNzaW9uX2lkPVwiK0RlcGxveS5zZXNzaW9uX2lkXG4gICAgICAgIH1cbiAgICAgIH0pLnRoZW4ociA9PiB7IHJldHVybiByLmpzb24oKTsgfSlcbiAgfVxuXG4gIGhhbmRsZUVycm9ycyhlcnJvcjogUmVzcG9uc2UpIHtcbiAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeShlcnJvci5qc29uKCkpKTtcbiAgICByZXR1cm4gT2JzZXJ2YWJsZS50aHJvdyhlcnJvcik7XG4gIH1cbn1cbiJdfQ==