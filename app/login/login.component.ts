import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { User } from "../resources/user/user";
import { UserService } from "../resources/user/user.service";
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";
import { Deploy } from "../resources/deploy/deploy";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: "my-app",
  providers: [UserService],
  templateUrl: "login/login.html",
  styleUrls: ["login/login-common.css", "login/login.css"]
})

export class LoginComponent implements OnInit {
  user: User;
  isLoggingIn = true;
  
constructor(private router: Router, private userService: UserService, private page: Page) {
  this.user = new User();
  this.user.session_id = "1";
}

 submit() {
  if (this.isLoggingIn) {
    this.login();
  }
}

login() {
  this.userService.login(this.user).then((response) => {
    console.log(JSON.stringify(response));
    let copy =JSON.parse(JSON.stringify(response));
    if(copy.errCode==0){
        Deploy.session_id = copy.resource[0].session_id;
        console.log(Deploy.session_id);
        require( "nativescript-localstorage" );
        localStorage.setItem('session_id', Deploy.session_id);
        this.getCommunity();
    }else{
        alert(copy.message);
    };
});
}

getCommunity() {
  this.userService.getCommunity().then((response) => {
    let record =JSON.parse(JSON.stringify(response));
     if(record.errCode==0){
        Deploy.community_id = record.resource[0].community_id;
        this.router.navigate(["/home"]);  
    }else{
        alert(record.message);
    };
});
}

ngOnInit() {
    this.page.actionBarHidden = true;
    require( "nativescript-localstorage" );
    let session_id = localStorage.getItem('session_id');
    if(session_id!=null && session_id!=""){
      Deploy.session_id = session_id;     
        this.getCommunity();
    }
}

}
