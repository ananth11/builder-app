"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_1 = require("../resources/user/user");
var user_service_1 = require("../resources/user/user.service");
var page_1 = require("ui/page");
var deploy_1 = require("../resources/deploy/deploy");
var router_1 = require("@angular/router");
var LoginComponent = (function () {
    function LoginComponent(router, userService, page) {
        this.router = router;
        this.userService = userService;
        this.page = page;
        this.isLoggingIn = true;
        this.user = new user_1.User();
        this.user.session_id = "1";
    }
    LoginComponent.prototype.submit = function () {
        if (this.isLoggingIn) {
            this.login();
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.userService.login(this.user).then(function (response) {
            console.log(JSON.stringify(response));
            var copy = JSON.parse(JSON.stringify(response));
            if (copy.errCode == 0) {
                deploy_1.Deploy.session_id = copy.resource[0].session_id;
                console.log(deploy_1.Deploy.session_id);
                require("nativescript-localstorage");
                localStorage.setItem('session_id', deploy_1.Deploy.session_id);
                _this.getCommunity();
            }
            else {
                alert(copy.message);
            }
            ;
        });
    };
    LoginComponent.prototype.getCommunity = function () {
        var _this = this;
        this.userService.getCommunity().then(function (response) {
            var record = JSON.parse(JSON.stringify(response));
            if (record.errCode == 0) {
                deploy_1.Deploy.community_id = record.resource[0].community_id;
                _this.router.navigate(["/home"]);
            }
            else {
                alert(record.message);
            }
            ;
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        require("nativescript-localstorage");
        var session_id = localStorage.getItem('session_id');
        if (session_id != null && session_id != "") {
            deploy_1.Deploy.session_id = session_id;
            this.getCommunity();
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        providers: [user_service_1.UserService],
        templateUrl: "login/login.html",
        styleUrls: ["login/login-common.css", "login/login.css"]
    }),
    __metadata("design:paramtypes", [router_1.Router, user_service_1.UserService, page_1.Page])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLCtDQUE4QztBQUM5QywrREFBNkQ7QUFDN0QsZ0NBQStCO0FBRy9CLHFEQUFvRDtBQUNwRCwwQ0FBeUQ7QUFTekQsSUFBYSxjQUFjO0lBSTNCLHdCQUFvQixNQUFjLEVBQVUsV0FBd0IsRUFBVSxJQUFVO1FBQXBFLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFGdEYsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFHbkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFdBQUksRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQztJQUM3QixDQUFDO0lBRUEsK0JBQU0sR0FBTjtRQUNDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNmLENBQUM7SUFDSCxDQUFDO0lBRUQsOEJBQUssR0FBTDtRQUFBLGlCQWNDO1FBYkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLFFBQVE7WUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdEMsSUFBSSxJQUFJLEdBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDL0MsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBRSxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNoQixlQUFNLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO2dCQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDL0IsT0FBTyxDQUFFLDJCQUEyQixDQUFFLENBQUM7Z0JBQ3ZDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLGVBQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDdEQsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3hCLENBQUM7WUFBQSxJQUFJLENBQUEsQ0FBQztnQkFDRixLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3hCLENBQUM7WUFBQSxDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7SUFDSCxDQUFDO0lBRUQscUNBQVksR0FBWjtRQUFBLGlCQVVDO1FBVEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQzVDLElBQUksTUFBTSxHQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2hELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDbkIsZUFBTSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFDdEQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLENBQUM7WUFBQSxJQUFJLENBQUEsQ0FBQztnQkFDRixLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFCLENBQUM7WUFBQSxDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7SUFDSCxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUNqQyxPQUFPLENBQUUsMkJBQTJCLENBQUUsQ0FBQztRQUN2QyxJQUFJLFVBQVUsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BELEVBQUUsQ0FBQSxDQUFDLFVBQVUsSUFBRSxJQUFJLElBQUksVUFBVSxJQUFFLEVBQUUsQ0FBQyxDQUFBLENBQUM7WUFDckMsZUFBTSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7WUFDN0IsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7SUFDTCxDQUFDO0lBRUQscUJBQUM7QUFBRCxDQUFDLEFBckRELElBcURDO0FBckRZLGNBQWM7SUFQMUIsZ0JBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxRQUFRO1FBQ2xCLFNBQVMsRUFBRSxDQUFDLDBCQUFXLENBQUM7UUFDeEIsV0FBVyxFQUFFLGtCQUFrQjtRQUMvQixTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsRUFBRSxpQkFBaUIsQ0FBQztLQUN6RCxDQUFDO3FDQU0wQixlQUFNLEVBQXVCLDBCQUFXLEVBQWdCLFdBQUk7R0FKM0UsY0FBYyxDQXFEMUI7QUFyRFksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vcmVzb3VyY2VzL3VzZXIvdXNlclwiO1xuaW1wb3J0IHsgVXNlclNlcnZpY2UgfSBmcm9tIFwiLi4vcmVzb3VyY2VzL3VzZXIvdXNlci5zZXJ2aWNlXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcImNvbG9yXCI7XG5pbXBvcnQgeyBWaWV3IH0gZnJvbSBcInVpL2NvcmUvdmlld1wiO1xuaW1wb3J0IHsgRGVwbG95IH0gZnJvbSBcIi4uL3Jlc291cmNlcy9kZXBsb3kvZGVwbG95XCI7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcIm15LWFwcFwiLFxuICBwcm92aWRlcnM6IFtVc2VyU2VydmljZV0sXG4gIHRlbXBsYXRlVXJsOiBcImxvZ2luL2xvZ2luLmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCJsb2dpbi9sb2dpbi1jb21tb24uY3NzXCIsIFwibG9naW4vbG9naW4uY3NzXCJdXG59KVxuXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICB1c2VyOiBVc2VyO1xuICBpc0xvZ2dpbmdJbiA9IHRydWU7XG4gIFxuY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSB1c2VyU2VydmljZTogVXNlclNlcnZpY2UsIHByaXZhdGUgcGFnZTogUGFnZSkge1xuICB0aGlzLnVzZXIgPSBuZXcgVXNlcigpO1xuICB0aGlzLnVzZXIuc2Vzc2lvbl9pZCA9IFwiMVwiO1xufVxuXG4gc3VibWl0KCkge1xuICBpZiAodGhpcy5pc0xvZ2dpbmdJbikge1xuICAgIHRoaXMubG9naW4oKTtcbiAgfVxufVxuXG5sb2dpbigpIHtcbiAgdGhpcy51c2VyU2VydmljZS5sb2dpbih0aGlzLnVzZXIpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcbiAgICBsZXQgY29weSA9SlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xuICAgIGlmKGNvcHkuZXJyQ29kZT09MCl7XG4gICAgICAgIERlcGxveS5zZXNzaW9uX2lkID0gY29weS5yZXNvdXJjZVswXS5zZXNzaW9uX2lkO1xuICAgICAgICBjb25zb2xlLmxvZyhEZXBsb3kuc2Vzc2lvbl9pZCk7XG4gICAgICAgIHJlcXVpcmUoIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiICk7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdzZXNzaW9uX2lkJywgRGVwbG95LnNlc3Npb25faWQpO1xuICAgICAgICB0aGlzLmdldENvbW11bml0eSgpO1xuICAgIH1lbHNle1xuICAgICAgICBhbGVydChjb3B5Lm1lc3NhZ2UpO1xuICAgIH07XG59KTtcbn1cblxuZ2V0Q29tbXVuaXR5KCkge1xuICB0aGlzLnVzZXJTZXJ2aWNlLmdldENvbW11bml0eSgpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgbGV0IHJlY29yZCA9SlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xuICAgICBpZihyZWNvcmQuZXJyQ29kZT09MCl7XG4gICAgICAgIERlcGxveS5jb21tdW5pdHlfaWQgPSByZWNvcmQucmVzb3VyY2VbMF0uY29tbXVuaXR5X2lkO1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvaG9tZVwiXSk7ICBcbiAgICB9ZWxzZXtcbiAgICAgICAgYWxlcnQocmVjb3JkLm1lc3NhZ2UpO1xuICAgIH07XG59KTtcbn1cblxubmdPbkluaXQoKSB7XG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IHRydWU7XG4gICAgcmVxdWlyZSggXCJuYXRpdmVzY3JpcHQtbG9jYWxzdG9yYWdlXCIgKTtcbiAgICBsZXQgc2Vzc2lvbl9pZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzZXNzaW9uX2lkJyk7XG4gICAgaWYoc2Vzc2lvbl9pZCE9bnVsbCAmJiBzZXNzaW9uX2lkIT1cIlwiKXtcbiAgICAgIERlcGxveS5zZXNzaW9uX2lkID0gc2Vzc2lvbl9pZDsgICAgIFxuICAgICAgICB0aGlzLmdldENvbW11bml0eSgpO1xuICAgIH1cbn1cblxufVxuIl19