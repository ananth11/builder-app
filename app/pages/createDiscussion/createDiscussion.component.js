"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var createDiscussionComponent = (function () {
    function createDiscussionComponent(router) {
        this.router = router;
    }
    createDiscussionComponent.prototype.onNavBtnTap = function () {
        this.router.navigate(["SelGrp"]);
    };
    return createDiscussionComponent;
}());
createDiscussionComponent = __decorate([
    core_1.Component({
        selector: "cd",
        templateUrl: "pages/createDiscussion/createDiscussion.html",
    }),
    __metadata("design:paramtypes", [router_1.Router])
], createDiscussionComponent);
exports.createDiscussionComponent = createDiscussionComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3JlYXRlRGlzY3Vzc2lvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjcmVhdGVEaXNjdXNzaW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUMxQywwQ0FBeUM7QUFNekMsSUFBYSx5QkFBeUI7SUFDbEMsbUNBQW9CLE1BQWM7UUFBZCxXQUFNLEdBQU4sTUFBTSxDQUFRO0lBRWxDLENBQUM7SUFDTSwrQ0FBVyxHQUFsQjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBQ0wsZ0NBQUM7QUFBRCxDQUFDLEFBUEQsSUFPQztBQVBZLHlCQUF5QjtJQUxyQyxnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLElBQUk7UUFDZCxXQUFXLEVBQUUsOENBQThDO0tBQzVELENBQUM7cUNBRzhCLGVBQU07R0FEekIseUJBQXlCLENBT3JDO0FBUFksOERBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJjZFwiLFxuICB0ZW1wbGF0ZVVybDogXCJwYWdlcy9jcmVhdGVEaXNjdXNzaW9uL2NyZWF0ZURpc2N1c3Npb24uaHRtbFwiLFxufSlcblxuZXhwb3J0IGNsYXNzIGNyZWF0ZURpc2N1c3Npb25Db21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpe1xuXG4gICAgfVxuICAgIHB1YmxpYyBvbk5hdkJ0blRhcCgpe1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCJTZWxHcnBcIl0pO1xuICAgIH1cbn0iXX0=