import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "cd",
  templateUrl: "pages/createDiscussion/createDiscussion.html",
})

export class createDiscussionComponent {
    constructor(private router: Router){

    }
    public onNavBtnTap(){
        this.router.navigate(["SelGrp"]);
    }
}