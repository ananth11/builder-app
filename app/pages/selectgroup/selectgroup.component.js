"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var SelectgroupComponent = (function () {
    function SelectgroupComponent(router) {
        this.router = router;
    }
    SelectgroupComponent.prototype.createDiscussion = function () {
        this.router.navigate(["CreDis"]);
    };
    SelectgroupComponent.prototype.Gohome = function () {
        this.router.navigate(["home"]);
    };
    return SelectgroupComponent;
}());
SelectgroupComponent = __decorate([
    core_1.Component({
        selector: "sg",
        templateUrl: "pages/selectgroup/selectgroup.html",
    }),
    __metadata("design:paramtypes", [router_1.Router])
], SelectgroupComponent);
exports.SelectgroupComponent = SelectgroupComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0Z3JvdXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2VsZWN0Z3JvdXAuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLDBDQUF5QztBQU16QyxJQUFhLG9CQUFvQjtJQUM3Qiw4QkFBb0IsTUFBYztRQUFkLFdBQU0sR0FBTixNQUFNLENBQVE7SUFFbEMsQ0FBQztJQUNNLCtDQUFnQixHQUF2QjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQTtJQUNwQyxDQUFDO0lBQ00scUNBQU0sR0FBYjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQTtJQUNsQyxDQUFDO0lBQ0wsMkJBQUM7QUFBRCxDQUFDLEFBVkQsSUFVQztBQVZZLG9CQUFvQjtJQUxoQyxnQkFBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLElBQUk7UUFDZCxXQUFXLEVBQUUsb0NBQW9DO0tBQ2xELENBQUM7cUNBRzhCLGVBQU07R0FEekIsb0JBQW9CLENBVWhDO0FBVlksb0RBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogXCJzZ1wiLFxuICB0ZW1wbGF0ZVVybDogXCJwYWdlcy9zZWxlY3Rncm91cC9zZWxlY3Rncm91cC5odG1sXCIsXG59KVxuXG5leHBvcnQgY2xhc3MgU2VsZWN0Z3JvdXBDb21wb25lbnQge1xuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpe1xuICAgICAgICBcbiAgICB9XG4gICAgcHVibGljIGNyZWF0ZURpc2N1c3Npb24oKXtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiQ3JlRGlzXCJdKVxuICAgIH1cbiAgICBwdWJsaWMgR29ob21lKCl7XG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcImhvbWVcIl0pXG4gICAgfVxufVxuIl19