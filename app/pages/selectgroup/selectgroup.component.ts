import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "sg",
  templateUrl: "pages/selectgroup/selectgroup.html",
})

export class SelectgroupComponent {
    constructor(private router: Router){
        
    }
    public createDiscussion(){
        this.router.navigate(["CreDis"])
    }
    public Gohome(){
        this.router.navigate(["home"])
    }
}
