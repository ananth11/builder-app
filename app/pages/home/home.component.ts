import { Component, ElementRef,AfterViewInit,ChangeDetectorRef, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "ui/page";
import { Color } from "color";
import { View } from "ui/core/view";
import { Deploy } from "../../resources/deploy/deploy";
import * as tabViewModule from 'tns-core-modules/ui/tab-view';
import * as elementRegistryModule from 'nativescript-angular/element-registry';
import { HomeService } from "../../resources/home/home.service";
import { C4t_object } from "../../resources/home/c4t_object";
import { Label } from "ui/label";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-telerik-ui/sidedrawer/angular";
import { ActionItem } from "ui/action-bar";
import { Observable } from "data/observable";
import { RadSideDrawer } from 'nativescript-telerik-ui/sidedrawer';
import { registerElement } from "nativescript-angular/element-registry";

registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);
//import { userListComponent } from './components/list/list.component';
elementRegistryModule.registerElement("CardView", () => require("nativescript-cardview").CardView);
@Component({
  //moduleId: module.id,
  providers: [HomeService],
  templateUrl: "pages/home/home.html",
  styleUrls: ["pages/home/home-common.css"]
})

export class HomeComponent implements OnInit {
//    @ViewChild(RadSideDrawerComponent)
//     public drawerComponent: RadSideDrawerComponent;
//     private drawer: SideDrawerType;
    //public pages:Array<Object>;
   c4t_object: C4t_object;
   num_contacts;
   num_staff;
   num_pets;
   _notice1= "";
   _notice2= "";
   my_discussion1 = "";
   my_discussion2 = "";
   my_discussion3 = "";
   _discussion1 = "";
   _discussion2 = "";
   _discussion3 = "";
  visible1 = "visible"
  visible2 = "visible"
  visible3 = "visible"
  visible11 = "visible"
  visible22 = "visible"
  visible33 = "visible"
 constructor(private router: Router,private page: Page,private homeService: HomeService) {

 }

   logout() {
        require( "nativescript-localstorage" );
        localStorage.removeItem('session_id');
        Deploy.session_id = "";
        alert("Successfully LoggedOut");
        this.router.navigate(["/logout"]);
    }

    getDiscussion() {
        this.homeService.getDiscussion().then((response) => {
            let record =JSON.parse(JSON.stringify(response));
            console.log(JSON.stringify(response));
            if(record.errCode==0){
                if(record.resource.length==1){
                    this.visible22 = "collapsed"
                    this.visible33 = "collapsed"
                    this._discussion1 = record.resource[0].title
                }
                if(record.resource.length==2){
                    this.visible33 = "collapsed"
                    this._discussion1 = record.resource[0].title
                    this._discussion2 = record.resource[1].title
                }
                if(record.resource.length==3){
                    this._discussion1 = record.resource[0].title
                    this._discussion2 = record.resource[1].title
                    this._discussion3 = record.resource[2].title
                }
            }else{
                alert(record.message);
            };
        })
    }

    getMyDiscussion() {
        this.homeService.getMyDiscussion().then((response) => {
             console.log(JSON.stringify(response));
            let record =JSON.parse(JSON.stringify(response));
            if(record.errCode==0){
                if(record.resource.length==1){
                    this.visible2 = "collapsed"
                    this.visible3 = "collapsed"
                    this.my_discussion1 = record.resource[0].title
                }
                if(record.resource.length==2){
                    this.visible3 = "collapsed"
                    this.my_discussion1 = record.resource[0].title
                    this.my_discussion2 = record.resource[1].title
                }
                if(record.resource.length==3){
                    this.my_discussion1 = record.resource[0].title
                    this.my_discussion2 = record.resource[1].title
                    this.my_discussion3 = record.resource[2].title
                }
            }else{
                alert(record.message);
            };
        });
    }
    public OnTap(){
        this.router.navigate(["/SelGrp"]);
    }

    ngOnInit() {
    this.page.actionBarHidden = false;
    this.getDiscussion();
    this.getMyDiscussion();
    //this.drawer = this.drawerComponent.sideDrawer;
    }
    // ngAfterViewInit() {
    //     this.drawer = this.drawerComponent.sideDrawer;
    //     this._changeDetectionRef.detectChanges();
    // }

    public openDrawer() {
        //this.drawer.toggleDrawerState();
        console.log("hey");
    }
    
}

