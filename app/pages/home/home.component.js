"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var page_1 = require("ui/page");
var deploy_1 = require("../../resources/deploy/deploy");
var elementRegistryModule = require("nativescript-angular/element-registry");
var home_service_1 = require("../../resources/home/home.service");
var element_registry_1 = require("nativescript-angular/element-registry");
element_registry_1.registerElement("Fab", function () { return require("nativescript-floatingactionbutton").Fab; });
//import { userListComponent } from './components/list/list.component';
elementRegistryModule.registerElement("CardView", function () { return require("nativescript-cardview").CardView; });
var HomeComponent = (function () {
    function HomeComponent(router, page, homeService) {
        this.router = router;
        this.page = page;
        this.homeService = homeService;
        this._notice1 = "";
        this._notice2 = "";
        this.my_discussion1 = "";
        this.my_discussion2 = "";
        this.my_discussion3 = "";
        this._discussion1 = "";
        this._discussion2 = "";
        this._discussion3 = "";
        this.visible1 = "visible";
        this.visible2 = "visible";
        this.visible3 = "visible";
        this.visible11 = "visible";
        this.visible22 = "visible";
        this.visible33 = "visible";
    }
    HomeComponent.prototype.logout = function () {
        require("nativescript-localstorage");
        localStorage.removeItem('session_id');
        deploy_1.Deploy.session_id = "";
        alert("Successfully LoggedOut");
        this.router.navigate(["/logout"]);
    };
    HomeComponent.prototype.getDiscussion = function () {
        var _this = this;
        this.homeService.getDiscussion().then(function (response) {
            var record = JSON.parse(JSON.stringify(response));
            console.log(JSON.stringify(response));
            if (record.errCode == 0) {
                if (record.resource.length == 1) {
                    _this.visible22 = "collapsed";
                    _this.visible33 = "collapsed";
                    _this._discussion1 = record.resource[0].title;
                }
                if (record.resource.length == 2) {
                    _this.visible33 = "collapsed";
                    _this._discussion1 = record.resource[0].title;
                    _this._discussion2 = record.resource[1].title;
                }
                if (record.resource.length == 3) {
                    _this._discussion1 = record.resource[0].title;
                    _this._discussion2 = record.resource[1].title;
                    _this._discussion3 = record.resource[2].title;
                }
            }
            else {
                alert(record.message);
            }
            ;
        });
    };
    HomeComponent.prototype.getMyDiscussion = function () {
        var _this = this;
        this.homeService.getMyDiscussion().then(function (response) {
            console.log(JSON.stringify(response));
            var record = JSON.parse(JSON.stringify(response));
            if (record.errCode == 0) {
                if (record.resource.length == 1) {
                    _this.visible2 = "collapsed";
                    _this.visible3 = "collapsed";
                    _this.my_discussion1 = record.resource[0].title;
                }
                if (record.resource.length == 2) {
                    _this.visible3 = "collapsed";
                    _this.my_discussion1 = record.resource[0].title;
                    _this.my_discussion2 = record.resource[1].title;
                }
                if (record.resource.length == 3) {
                    _this.my_discussion1 = record.resource[0].title;
                    _this.my_discussion2 = record.resource[1].title;
                    _this.my_discussion3 = record.resource[2].title;
                }
            }
            else {
                alert(record.message);
            }
            ;
        });
    };
    HomeComponent.prototype.OnTap = function () {
        this.router.navigate(["/SelGrp"]);
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = false;
        this.getDiscussion();
        this.getMyDiscussion();
        //this.drawer = this.drawerComponent.sideDrawer;
    };
    // ngAfterViewInit() {
    //     this.drawer = this.drawerComponent.sideDrawer;
    //     this._changeDetectionRef.detectChanges();
    // }
    HomeComponent.prototype.openDrawer = function () {
        //this.drawer.toggleDrawerState();
        console.log("hey");
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        //moduleId: module.id,
        providers: [home_service_1.HomeService],
        templateUrl: "pages/home/home.html",
        styleUrls: ["pages/home/home-common.css"]
    }),
    __metadata("design:paramtypes", [router_1.Router, page_1.Page, home_service_1.HomeService])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5RztBQUN6RywwQ0FBeUM7QUFDekMsZ0NBQStCO0FBRy9CLHdEQUF1RDtBQUV2RCw2RUFBK0U7QUFDL0Usa0VBQWdFO0FBT2hFLDBFQUF3RTtBQUV4RSxrQ0FBZSxDQUFDLEtBQUssRUFBRSxjQUFNLE9BQUEsT0FBTyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsR0FBRyxFQUFoRCxDQUFnRCxDQUFDLENBQUM7QUFDL0UsdUVBQXVFO0FBQ3ZFLHFCQUFxQixDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsY0FBTSxPQUFBLE9BQU8sQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLFFBQVEsRUFBekMsQ0FBeUMsQ0FBQyxDQUFDO0FBUW5HLElBQWEsYUFBYTtJQXVCekIsdUJBQW9CLE1BQWMsRUFBUyxJQUFVLEVBQVMsV0FBd0I7UUFBbEUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFTLFNBQUksR0FBSixJQUFJLENBQU07UUFBUyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQWRwRixhQUFRLEdBQUUsRUFBRSxDQUFDO1FBQ2IsYUFBUSxHQUFFLEVBQUUsQ0FBQztRQUNiLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ25CLGFBQVEsR0FBRyxTQUFTLENBQUE7UUFDcEIsYUFBUSxHQUFHLFNBQVMsQ0FBQTtRQUNwQixhQUFRLEdBQUcsU0FBUyxDQUFBO1FBQ3BCLGNBQVMsR0FBRyxTQUFTLENBQUE7UUFDckIsY0FBUyxHQUFHLFNBQVMsQ0FBQTtRQUNyQixjQUFTLEdBQUcsU0FBUyxDQUFBO0lBR3RCLENBQUM7SUFFQyw4QkFBTSxHQUFOO1FBQ0ssT0FBTyxDQUFFLDJCQUEyQixDQUFFLENBQUM7UUFDdkMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN0QyxlQUFNLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELHFDQUFhLEdBQWI7UUFBQSxpQkF3QkM7UUF2QkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQzNDLElBQUksTUFBTSxHQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3RDLEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDbEIsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDMUIsS0FBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUE7b0JBQzVCLEtBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFBO29CQUM1QixLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO2dCQUNoRCxDQUFDO2dCQUNELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFBO29CQUM1QixLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO29CQUM1QyxLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO2dCQUNoRCxDQUFDO2dCQUNELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7b0JBQzVDLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7b0JBQzVDLEtBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7Z0JBQ2hELENBQUM7WUFDTCxDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQixDQUFDO1lBQUEsQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELHVDQUFlLEdBQWY7UUFBQSxpQkF3QkM7UUF2QkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFRO1lBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksTUFBTSxHQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ2pELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxPQUFPLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztnQkFDbEIsRUFBRSxDQUFBLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQztvQkFDMUIsS0FBSSxDQUFDLFFBQVEsR0FBRyxXQUFXLENBQUE7b0JBQzNCLEtBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFBO29CQUMzQixLQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO2dCQUNsRCxDQUFDO2dCQUNELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFBO29CQUMzQixLQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO29CQUM5QyxLQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO2dCQUNsRCxDQUFDO2dCQUNELEVBQUUsQ0FBQSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFFLENBQUMsQ0FBQyxDQUFBLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7b0JBQzlDLEtBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7b0JBQzlDLEtBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUE7Z0JBQ2xELENBQUM7WUFDTCxDQUFDO1lBQUEsSUFBSSxDQUFBLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMxQixDQUFDO1lBQUEsQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNNLDZCQUFLLEdBQVo7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDQSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixnREFBZ0Q7SUFDaEQsQ0FBQztJQUNELHNCQUFzQjtJQUN0QixxREFBcUQ7SUFDckQsZ0RBQWdEO0lBQ2hELElBQUk7SUFFRyxrQ0FBVSxHQUFqQjtRQUNJLGtDQUFrQztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFTCxvQkFBQztBQUFELENBQUMsQUExR0QsSUEwR0M7QUExR1ksYUFBYTtJQVB6QixnQkFBUyxDQUFDO1FBQ1Qsc0JBQXNCO1FBQ3RCLFNBQVMsRUFBRSxDQUFDLDBCQUFXLENBQUM7UUFDeEIsV0FBVyxFQUFFLHNCQUFzQjtRQUNuQyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQztLQUMxQyxDQUFDO3FDQXlCMkIsZUFBTSxFQUFlLFdBQUksRUFBc0IsMEJBQVc7R0F2QjFFLGFBQWEsQ0EwR3pCO0FBMUdZLHNDQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLEFmdGVyVmlld0luaXQsQ2hhbmdlRGV0ZWN0b3JSZWYsIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgQ29sb3IgfSBmcm9tIFwiY29sb3JcIjtcbmltcG9ydCB7IFZpZXcgfSBmcm9tIFwidWkvY29yZS92aWV3XCI7XG5pbXBvcnQgeyBEZXBsb3kgfSBmcm9tIFwiLi4vLi4vcmVzb3VyY2VzL2RlcGxveS9kZXBsb3lcIjtcbmltcG9ydCAqIGFzIHRhYlZpZXdNb2R1bGUgZnJvbSAndG5zLWNvcmUtbW9kdWxlcy91aS90YWItdmlldyc7XG5pbXBvcnQgKiBhcyBlbGVtZW50UmVnaXN0cnlNb2R1bGUgZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvZWxlbWVudC1yZWdpc3RyeSc7XG5pbXBvcnQgeyBIb21lU2VydmljZSB9IGZyb20gXCIuLi8uLi9yZXNvdXJjZXMvaG9tZS9ob21lLnNlcnZpY2VcIjtcbmltcG9ydCB7IEM0dF9vYmplY3QgfSBmcm9tIFwiLi4vLi4vcmVzb3VyY2VzL2hvbWUvYzR0X29iamVjdFwiO1xuaW1wb3J0IHsgTGFiZWwgfSBmcm9tIFwidWkvbGFiZWxcIjtcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXJDb21wb25lbnQsIFNpZGVEcmF3ZXJUeXBlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC10ZWxlcmlrLXVpL3NpZGVkcmF3ZXIvYW5ndWxhclwiO1xuaW1wb3J0IHsgQWN0aW9uSXRlbSB9IGZyb20gXCJ1aS9hY3Rpb24tYmFyXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcImRhdGEvb2JzZXJ2YWJsZVwiO1xuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gJ25hdGl2ZXNjcmlwdC10ZWxlcmlrLXVpL3NpZGVkcmF3ZXInO1xuaW1wb3J0IHsgcmVnaXN0ZXJFbGVtZW50IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL2VsZW1lbnQtcmVnaXN0cnlcIjtcblxucmVnaXN0ZXJFbGVtZW50KFwiRmFiXCIsICgpID0+IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtZmxvYXRpbmdhY3Rpb25idXR0b25cIikuRmFiKTtcbi8vaW1wb3J0IHsgdXNlckxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbGlzdC9saXN0LmNvbXBvbmVudCc7XG5lbGVtZW50UmVnaXN0cnlNb2R1bGUucmVnaXN0ZXJFbGVtZW50KFwiQ2FyZFZpZXdcIiwgKCkgPT4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1jYXJkdmlld1wiKS5DYXJkVmlldyk7XG5AQ29tcG9uZW50KHtcbiAgLy9tb2R1bGVJZDogbW9kdWxlLmlkLFxuICBwcm92aWRlcnM6IFtIb21lU2VydmljZV0sXG4gIHRlbXBsYXRlVXJsOiBcInBhZ2VzL2hvbWUvaG9tZS5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wicGFnZXMvaG9tZS9ob21lLWNvbW1vbi5jc3NcIl1cbn0pXG5cbmV4cG9ydCBjbGFzcyBIb21lQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbi8vICAgIEBWaWV3Q2hpbGQoUmFkU2lkZURyYXdlckNvbXBvbmVudClcbi8vICAgICBwdWJsaWMgZHJhd2VyQ29tcG9uZW50OiBSYWRTaWRlRHJhd2VyQ29tcG9uZW50O1xuLy8gICAgIHByaXZhdGUgZHJhd2VyOiBTaWRlRHJhd2VyVHlwZTtcbiAgICAvL3B1YmxpYyBwYWdlczpBcnJheTxPYmplY3Q+O1xuICAgYzR0X29iamVjdDogQzR0X29iamVjdDtcbiAgIG51bV9jb250YWN0cztcbiAgIG51bV9zdGFmZjtcbiAgIG51bV9wZXRzO1xuICAgX25vdGljZTE9IFwiXCI7XG4gICBfbm90aWNlMj0gXCJcIjtcbiAgIG15X2Rpc2N1c3Npb24xID0gXCJcIjtcbiAgIG15X2Rpc2N1c3Npb24yID0gXCJcIjtcbiAgIG15X2Rpc2N1c3Npb24zID0gXCJcIjtcbiAgIF9kaXNjdXNzaW9uMSA9IFwiXCI7XG4gICBfZGlzY3Vzc2lvbjIgPSBcIlwiO1xuICAgX2Rpc2N1c3Npb24zID0gXCJcIjtcbiAgdmlzaWJsZTEgPSBcInZpc2libGVcIlxuICB2aXNpYmxlMiA9IFwidmlzaWJsZVwiXG4gIHZpc2libGUzID0gXCJ2aXNpYmxlXCJcbiAgdmlzaWJsZTExID0gXCJ2aXNpYmxlXCJcbiAgdmlzaWJsZTIyID0gXCJ2aXNpYmxlXCJcbiAgdmlzaWJsZTMzID0gXCJ2aXNpYmxlXCJcbiBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLHByaXZhdGUgcGFnZTogUGFnZSxwcml2YXRlIGhvbWVTZXJ2aWNlOiBIb21lU2VydmljZSkge1xuXG4gfVxuXG4gICBsb2dvdXQoKSB7XG4gICAgICAgIHJlcXVpcmUoIFwibmF0aXZlc2NyaXB0LWxvY2Fsc3RvcmFnZVwiICk7XG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdzZXNzaW9uX2lkJyk7XG4gICAgICAgIERlcGxveS5zZXNzaW9uX2lkID0gXCJcIjtcbiAgICAgICAgYWxlcnQoXCJTdWNjZXNzZnVsbHkgTG9nZ2VkT3V0XCIpO1xuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCIvbG9nb3V0XCJdKTtcbiAgICB9XG5cbiAgICBnZXREaXNjdXNzaW9uKCkge1xuICAgICAgICB0aGlzLmhvbWVTZXJ2aWNlLmdldERpc2N1c3Npb24oKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgbGV0IHJlY29yZCA9SlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShyZXNwb25zZSkpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcbiAgICAgICAgICAgIGlmKHJlY29yZC5lcnJDb2RlPT0wKXtcbiAgICAgICAgICAgICAgICBpZihyZWNvcmQucmVzb3VyY2UubGVuZ3RoPT0xKXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aXNpYmxlMjIgPSBcImNvbGxhcHNlZFwiXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlzaWJsZTMzID0gXCJjb2xsYXBzZWRcIlxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaXNjdXNzaW9uMSA9IHJlY29yZC5yZXNvdXJjZVswXS50aXRsZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZihyZWNvcmQucmVzb3VyY2UubGVuZ3RoPT0yKXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aXNpYmxlMzMgPSBcImNvbGxhcHNlZFwiXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2Rpc2N1c3Npb24xID0gcmVjb3JkLnJlc291cmNlWzBdLnRpdGxlXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2Rpc2N1c3Npb24yID0gcmVjb3JkLnJlc291cmNlWzFdLnRpdGxlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKHJlY29yZC5yZXNvdXJjZS5sZW5ndGg9PTMpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaXNjdXNzaW9uMSA9IHJlY29yZC5yZXNvdXJjZVswXS50aXRsZVxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaXNjdXNzaW9uMiA9IHJlY29yZC5yZXNvdXJjZVsxXS50aXRsZVxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaXNjdXNzaW9uMyA9IHJlY29yZC5yZXNvdXJjZVsyXS50aXRsZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIGFsZXJ0KHJlY29yZC5tZXNzYWdlKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pXG4gICAgfVxuXG4gICAgZ2V0TXlEaXNjdXNzaW9uKCkge1xuICAgICAgICB0aGlzLmhvbWVTZXJ2aWNlLmdldE15RGlzY3Vzc2lvbigpLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgY29uc29sZS5sb2coSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcbiAgICAgICAgICAgIGxldCByZWNvcmQgPUpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkocmVzcG9uc2UpKTtcbiAgICAgICAgICAgIGlmKHJlY29yZC5lcnJDb2RlPT0wKXtcbiAgICAgICAgICAgICAgICBpZihyZWNvcmQucmVzb3VyY2UubGVuZ3RoPT0xKXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aXNpYmxlMiA9IFwiY29sbGFwc2VkXCJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aXNpYmxlMyA9IFwiY29sbGFwc2VkXCJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teV9kaXNjdXNzaW9uMSA9IHJlY29yZC5yZXNvdXJjZVswXS50aXRsZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZihyZWNvcmQucmVzb3VyY2UubGVuZ3RoPT0yKXtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aXNpYmxlMyA9IFwiY29sbGFwc2VkXCJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teV9kaXNjdXNzaW9uMSA9IHJlY29yZC5yZXNvdXJjZVswXS50aXRsZVxuICAgICAgICAgICAgICAgICAgICB0aGlzLm15X2Rpc2N1c3Npb24yID0gcmVjb3JkLnJlc291cmNlWzFdLnRpdGxlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKHJlY29yZC5yZXNvdXJjZS5sZW5ndGg9PTMpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm15X2Rpc2N1c3Npb24xID0gcmVjb3JkLnJlc291cmNlWzBdLnRpdGxlXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubXlfZGlzY3Vzc2lvbjIgPSByZWNvcmQucmVzb3VyY2VbMV0udGl0bGVcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5teV9kaXNjdXNzaW9uMyA9IHJlY29yZC5yZXNvdXJjZVsyXS50aXRsZVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1lbHNle1xuICAgICAgICAgICAgICAgIGFsZXJ0KHJlY29yZC5tZXNzYWdlKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBwdWJsaWMgT25UYXAoKXtcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL1NlbEdycFwiXSk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5wYWdlLmFjdGlvbkJhckhpZGRlbiA9IGZhbHNlO1xuICAgIHRoaXMuZ2V0RGlzY3Vzc2lvbigpO1xuICAgIHRoaXMuZ2V0TXlEaXNjdXNzaW9uKCk7XG4gICAgLy90aGlzLmRyYXdlciA9IHRoaXMuZHJhd2VyQ29tcG9uZW50LnNpZGVEcmF3ZXI7XG4gICAgfVxuICAgIC8vIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcbiAgICAvLyAgICAgdGhpcy5kcmF3ZXIgPSB0aGlzLmRyYXdlckNvbXBvbmVudC5zaWRlRHJhd2VyO1xuICAgIC8vICAgICB0aGlzLl9jaGFuZ2VEZXRlY3Rpb25SZWYuZGV0ZWN0Q2hhbmdlcygpO1xuICAgIC8vIH1cblxuICAgIHB1YmxpYyBvcGVuRHJhd2VyKCkge1xuICAgICAgICAvL3RoaXMuZHJhd2VyLnRvZ2dsZURyYXdlclN0YXRlKCk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiaGV5XCIpO1xuICAgIH1cbiAgICBcbn1cblxuIl19